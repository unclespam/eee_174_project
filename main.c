#include "simpletools.h"




//Basic Light Level Detection code
int lightLevel;

int main()                    
{
  while(1)
  {
    high(9); //Charges Capacitor
    pause(1);
    lightLevel = rc_time(9, 1); //Takes reading at PIN 9
    
   
    print("%cLight Level = %d%c",
           HOME, lightLevel, CLREOL); //Print out light level value (high light seems to be low value)
		  
    pause(250); //Refresh rate perhaps?
  }
}